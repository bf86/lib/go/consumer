package consumer

import (
	"fmt"
	"log"
	"os"

	"github.com/streadway/amqp"
)

var (
	conn     *amqp.Connection
	channel  *amqp.Channel
	err      error
	consumer *Consumer
)

type Consumer struct {
	Queue            string
	ConnectionString string
	Options          *AMQPOptions
	Args             amqp.Table
}

type Consume func(amqp.Delivery)

func NewConsumer(queue string) *Consumer {
	c := &Consumer{
		Queue:            queue,
		ConnectionString: getConnectionString(),
	}
	consumer = c
	return c
}

func Set(c *Consumer) {
	consumer = c
}

func Listen(consume Consume) error {
	if consumer == nil {
		return fmt.Errorf("Consumer is not set!")
	}
	err = consumer.Validate()
	if err != nil {
		return fmt.Errorf("%v", err)
	}

	if consumer.Options == nil {
		consumer.Options = NewOptions()
		log.Default().Printf("No Options set, using defaults: %+v\n", consumer.Options)
	}

	conn, err = amqp.Dial(consumer.ConnectionString)
	if err != nil {
		return err
	}
	defer conn.Close()

	channel, err = conn.Channel()
	if err != nil {
		return err
	}
	defer channel.Close()

	messages, err := channel.Consume(
		consumer.Queue,
		"",
		consumer.Options.AutoAck,
		consumer.Options.Exclusive,
		consumer.Options.NoLocal,
		consumer.Options.NoWait,
		consumer.Args,
	)
	if err != nil {
		return err
	}

	listen := make(chan bool)
	log.Printf(" ==> Waiting for messages on %s. Press CTRL+C to exit", consumer.Queue)

	go func() {
		for m := range messages {
			consume(m)
		}
	}()

	<-listen

	return nil
}

func (c *Consumer) Validate() error {
	if c.Queue == "" {
		return fmt.Errorf("missing attribute: Queue")
	}
	if c.ConnectionString == "" {
		return fmt.Errorf("missing attribute: ConnectionString")
	}
	return nil
}

func getConnectionString() string {
	str := os.Getenv("AMQP_SERVER_URL")
	if str != "" {
		return str
	}
	return "amqp://guest:guest@localhost:5672/"
}
