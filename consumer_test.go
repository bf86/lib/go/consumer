package consumer

import (
	"testing"

	"github.com/streadway/amqp"
)

type Test struct {
	Name    string
	WantErr bool
}

func TestListen_Consumer(t *testing.T) {
	test := Test{
		Name:    "Consumer not set",
		WantErr: true,
	}
	t.Run(test.Name, func(t *testing.T) {
		var consume func(amqp.Delivery)
		if err := Listen(consume); (err != nil) != test.WantErr {
			t.Errorf("Listen() error = %v, WantErr %v", err, test.WantErr)
		}
	})
}
