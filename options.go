package consumer

type AMQPOptions struct {
	AutoAck   bool
	Exclusive bool
	NoLocal   bool
	NoWait    bool
}

func NewOptions() *AMQPOptions {
	return &AMQPOptions{
		AutoAck:   true,
		Exclusive: false,
		NoLocal:   false,
		NoWait:    false,
	}
}
