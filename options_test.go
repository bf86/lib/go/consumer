package consumer

import (
	"reflect"
	"testing"
)

func TestNewOptions(t *testing.T) {
	tests := []struct {
		name string
		want *AMQPOptions
	}{
		{name: "Set Defaults", want: &AMQPOptions{
			AutoAck:   true,
			Exclusive: false,
			NoLocal:   false,
			NoWait:    false,
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOptions(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOptions() = %v, want %v", got, tt.want)
			}
		})
	}
}
